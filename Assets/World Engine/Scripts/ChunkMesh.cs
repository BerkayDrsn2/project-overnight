﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace ProjectOvernight.WorldEngine
{
    public enum Direction
    {
        NORTH   = 0,
        EAST    = 1,
        SOUTH   = 2,
        WEST    = 3,
        UP      = 4,
        DOWN    = 5
    }

    public class ChunkMesh
    {
        #region STATICS
        private static readonly Vector3[] vertexData = new Vector3[8]
        {
            new Vector3(0, 0, 0), new Vector3(0, 1, 0), new Vector3(1, 1, 0), new Vector3(1, 0, 0),
            new Vector3(0, 0, 1), new Vector3(0, 1, 1), new Vector3(1, 1, 1), new Vector3(1, 0, 1)
        };

        private static readonly byte[,] vertexIndexData = new byte[6, 4]
        {
            { 7, 6, 5, 4 }, // NORTH
            { 3, 2, 6, 7 }, // EAST
            { 0, 1, 2, 3 }, // SOUTH
            { 4, 5, 1, 0 }, // WEST
            { 1, 5, 6, 2 }, // UP
            { 4, 0, 3, 7 }  // DOWN
        };

        private static readonly Vector3[] normalData = new Vector3[6]
        {
            Vector3.forward,  // NORTH
            Vector3.right,    // EAST      
            Vector3.back,     // SOUTH
            Vector3.left,     // WEST
            Vector3.up,       // UP
            Vector3.down      // DOWN
        };
        #endregion

        #region COMPONENTS
        #endregion

        #region GAMEOBJECTS
        #endregion

        #region PROPERTIES
        public Mesh Mesh
        {
            get { return mesh; }
        }
        #endregion

        #region VARIABLES
        private Map map;
        private Chunk chunk;

        private Mesh mesh;
        private byte[,,] visibilityFlag;
        #endregion

        public ChunkMesh(Map map, Chunk chunk)
        {
            this.map = map;
            this.chunk = chunk;
            
            visibilityFlag = GenerateVisibilityFlags();
            mesh = GenerateMesh(visibilityFlag);
        }

        private byte[,,] GenerateVisibilityFlags()
        {
            visibilityFlag = new byte[chunk.Size, chunk.Height, chunk.Size];

            for (byte y = 0; y < chunk.Height; y++)
            {
                for (byte z = 0; z < chunk.Size; z++)
                {
                    for (byte x = 0; x < chunk.Size; x++)
                    {
                        Vector3Int index = new Vector3Int(x, y, z);
                        if (chunk[index].ID != 0)
                        {
                            Block[] adjBlocks = new Block[6];

                            // North Block
                            if (index.z < chunk.Size - 1)
                                adjBlocks[0] = chunk[x, y, z + 1];
                            else
                            {
                                Chunk adjChunk = map[chunk.Index + Vector2Int.up];
                                if (adjChunk != null)
                                    adjBlocks[0] = adjChunk[x, y, 0];
                            }

                            // East Block
                            if (index.x < chunk.Size - 1)
                                adjBlocks[1] = chunk[x + 1, y, z];
                            else
                            {
                                Chunk adjChunk = map[chunk.Index + Vector2Int.right];
                                if (adjChunk != null)
                                    adjBlocks[1] = adjChunk[0, y, z];
                            }

                            // South Block
                            if (index.z > 0)
                                adjBlocks[2] = chunk[x, y, z - 1];
                            else
                            {
                                Chunk adjChunk = map[chunk.Index + Vector2Int.down];
                                if (adjChunk != null)
                                    adjBlocks[2] = adjChunk[x, y, chunk.Size - 1];
                            }

                            // West Block
                            if (index.x > 0)
                                adjBlocks[3] = chunk[x - 1, y, z];
                            else
                            {
                                Chunk adjChunk = map[chunk.Index + Vector2Int.left];
                                if (adjChunk != null)
                                    adjBlocks[3] = adjChunk[chunk.Size - 1, y, z];
                            }

                            // Up Block
                            if (index.y < chunk.Height - 1)
                                adjBlocks[4] = chunk[x, y + 1, z];
                            else
                                adjBlocks[4] = null;

                            // Down Block
                            if (index.y > 0)
                                adjBlocks[5] = chunk[x, y - 1, z];
                            else
                                adjBlocks[5] = null;

                            for (int i = 0; i < 6; i++)
                            {
                                if (adjBlocks[i] == null || (adjBlocks[i] != null && adjBlocks[i].ID == 0))
                                    visibilityFlag[x, y, z] |= (byte) (1 << i);
                            }
                        }
                    }
                }
            }

            return visibilityFlag;
        }

        private Mesh GenerateMesh(byte[,,] visibilityFlag)
        {
            List<Mesh> voxelFaces = new List<Mesh>();
            Array directions = Enum.GetValues(typeof(Direction));

            foreach (Direction direction in directions)
            {
                int directionMask = 1 << (int)direction;

                if (direction == Direction.DOWN || direction == Direction.UP)
                {
                    for (int y = 0; y < chunk.Height; y++)
                    {
                        for (int z = 0; z < chunk.Size; z++)
                        {
                            for (int x = 0; x < chunk.Size; x++)
                            {
                                if ((visibilityFlag[x, y, z] & directionMask) != 0)
                                {
                                    int sizeX = 0;
                                    int sizeZ = 1;

                                    int mz = z;
                                    int mx = x;

                                    for (mx = x; mx < chunk.Size && (visibilityFlag[mx, y, mz] & directionMask) != 0; mx++)
                                    {
                                        sizeX++;
                                    }

                                    for (mz = z + 1, mx = x; mz < chunk.Size && mx < x + sizeX; mx++)
                                    {
                                        if ((visibilityFlag[mx, y, mz] & directionMask) == 0)
                                            break;

                                        if (mx == x + sizeX - 1)
                                        {
                                            sizeZ++;
                                            mz++;
                                            mx = x - 1;
                                        }
                                    }

                                    for (mz = z; mz < z + sizeZ; mz++)
                                    {
                                        for (mx = x; mx < x + sizeX; mx++)
                                        {
                                            visibilityFlag[mx, y, mz] &= (byte)~directionMask;
                                        }
                                    }

                                    Mesh voxelFace = GetVoxelFace(direction, new Vector3(x, y, z), new Vector2(sizeX, sizeZ));
                                    voxelFaces.Add(voxelFace);

                                    x = mx - 1;
                                }
                            }
                        }
                    }
                }
                else if (direction == Direction.EAST || direction == Direction.WEST)
                {
                    for (int x = 0; x < chunk.Size; x++)
                    {
                        for (int y = 0; y < chunk.Height; y++)
                        {
                            for (int z = 0; z < chunk.Size; z++)
                            {
                                if ((visibilityFlag[x, y, z] & directionMask) != 0)
                                {
                                    int sizeZ = 0;
                                    int sizeY = 1;

                                    int mz = z;
                                    int my = y;

                                    for (mz = z; mz < chunk.Size && (visibilityFlag[x, my, mz] & directionMask) != 0; mz++)
                                    {
                                        sizeZ++;
                                    }

                                    for (my = y + 1, mz = z; my < chunk.Height && mz < z + sizeZ; mz++)
                                    {
                                        if ((visibilityFlag[x, my, mz] & directionMask) == 0)
                                            break;

                                        if (mz == z + sizeZ - 1)
                                        {
                                            sizeY++;
                                            my++;
                                            mz = z - 1;
                                        }
                                    }

                                    for (my = y; my < y + sizeY; my++)
                                    {
                                        for (mz = z; mz < z + sizeZ; mz++)
                                        {
                                            visibilityFlag[x, my, mz] &= (byte)~directionMask;
                                        }
                                    }

                                    Mesh voxelFace = GetVoxelFace(direction, new Vector3(x, y, z), new Vector2(sizeZ, sizeY));
                                    voxelFaces.Add(voxelFace);

                                    z = mz - 1;
                                }
                            }
                        }
                    }
                }
                else
                {
                    for (int z = 0; z < chunk.Size; z++)
                    {
                        for (int y = 0; y < chunk.Height; y++)
                        {
                            for (int x = 0; x < chunk.Size; x++)
                            {
                                if ((visibilityFlag[x, y, z] & directionMask) != 0)
                                {
                                    int sizeX = 0;
                                    int sizeY = 1;

                                    int mx = x;
                                    int my = y;

                                    for (mx = x; mx < chunk.Size && (visibilityFlag[mx, my, z] & directionMask) != 0; mx++)
                                        sizeX++;

                                    for (my = y + 1, mx = x; my < chunk.Height && mx < x + sizeX; mx++)
                                    {
                                        if ((visibilityFlag[mx, my, z] & directionMask) == 0)
                                            break;

                                        if (mx == x + sizeX - 1)
                                        {
                                            sizeY++;
                                            my++;
                                            mx = x - 1;
                                        }
                                    }

                                    for (my = y; my < y + sizeY; my++)
                                    {
                                        for (mx = x; mx < x + sizeX; mx++)
                                        {
                                            visibilityFlag[mx, my, z] &= (byte)~directionMask;
                                        }
                                    }

                                    Mesh voxelFace = GetVoxelFace(direction, new Vector3(x, y, z), new Vector2(sizeX, sizeY));
                                    voxelFaces.Add(voxelFace);

                                    x = mx - 1;
                                }
                            }
                        }
                    }
                }
            }

            List<CombineInstance> combineInstances = new List<CombineInstance>();
            foreach (Mesh voxelFace in voxelFaces)
            {
                CombineInstance instance = new CombineInstance()
                {
                    mesh = voxelFace,
                    transform = GameObject.Find("World Engine").transform.localToWorldMatrix
                };

                combineInstances.Add(instance);
            }

            mesh = new Mesh();
            mesh.CombineMeshes(combineInstances.ToArray());
            mesh.name = "Chunk " + chunk.Index;

            return mesh;
        }

        private Mesh GetVoxelFace(Direction direction, Vector3 position, Vector2 size)
        {
            Mesh faceMesh = new Mesh();

            Vector3[] vertices = new Vector3[4];
            for (int i = 0; i < 4; i++)
            {
                Vector3 vertex = vertexData[vertexIndexData[(int)direction, i]];
                if (direction == Direction.UP || direction == Direction.DOWN)
                {
                    vertex.x *= size.x;
                    vertex.z *= size.y;
                }
                else if (direction == Direction.NORTH || direction == Direction.SOUTH)
                {
                    vertex.x *= size.x;
                    vertex.y *= size.y;
                }
                else
                {
                    vertex.z *= size.x;
                    vertex.y *= size.y;
                }
                vertices[i] = position + vertex;
            }

            int[] triangles = new int[6] { 0, 1, 2, 0, 2, 3 };

            Vector3 normal = normalData[(int)direction];
            Vector3[] normals =
            {
                normal,
                normal,
                normal,
                normal
            };

            Vector2[] uv =
            {
                new Vector2(0, 0),
                new Vector2(size.y, 0),
                new Vector2(size.y, size.x),
                new Vector2(0, size.x)
            };

            faceMesh.vertices = vertices;
            faceMesh.triangles = triangles;
            faceMesh.normals = normals;
            faceMesh.uv = uv;

            return faceMesh;
        }
    }
}
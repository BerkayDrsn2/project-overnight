﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProjectOvernight.WorldEngine
{
    [Serializable]
    public class ChunkManager
    {
        #region STATICS
        #endregion

        #region COMPONENTS
        private readonly World world;
        private readonly Map map;
        private readonly WorldGenerator worldGenerator;
        #endregion

        #region GAMEOBJECTS
        #endregion

        #region PROPERTIES
        public Vector2Int FocusPoint
        {
            get { return focusPoint; }
            set
            {
                if (focusPoint != value)
                {
                    focusPoint = value;
                    Update();
                }
            }
        }
        #endregion

        #region VARIABLES
        private Vector2Int focusPoint;

        private Dictionary<Vector2Int, GameObject> chunkObjectMap;

        private List<Vector2Int> visibleChunkList;
        private readonly Queue<Vector2Int> loadChunkQueue;
        private readonly Queue<Vector2Int> instantiateChunkQueue;
        private readonly Queue<Vector2Int> updateChunkQueue;

        private bool loadChunkCoroutineRunning;
        private bool instantiateChunkCoroutineRunning;
        private bool updateChunkCoroutineRunning;
        #endregion
        
        public ChunkManager(World world, WorldGenerator worldGenerator)
        {
            this.world = world;

            chunkObjectMap = new Dictionary<Vector2Int, GameObject>();
            loadChunkQueue = new Queue<Vector2Int>();
            instantiateChunkQueue = new Queue<Vector2Int>();
            updateChunkQueue = new Queue<Vector2Int>();

            map = world.GetMap();
            this.worldGenerator = worldGenerator;
        }

        public void Update()
        {
            UpdateVisibleChunkList();
            UpdateLoadChunkQueue();

            if (!loadChunkCoroutineRunning && loadChunkQueue.Count > 0)
            {
                world.StartCoroutine(LoadChunksCoroutine());
            }

            if (!instantiateChunkCoroutineRunning && instantiateChunkQueue.Count > 0)
            {
                world.StartCoroutine(InstantiateChunksCoroutine());
            }

            if (!updateChunkCoroutineRunning && updateChunkQueue.Count > 0)
            {
                world.StartCoroutine(UpdateChunksCoroutine());
            }
        }

        private void UpdateVisibleChunkList()
        {
            visibleChunkList = new List<Vector2Int>();

            for (int z = -world.Config.RenderDistance; z <= world.Config.RenderDistance; z++)
            {
                for (int x = -world.Config.RenderDistance; x <= world.Config.RenderDistance; x++)
                {
                    Vector2Int chunkIndex = focusPoint + new Vector2Int(x, z);

                    visibleChunkList.Add(chunkIndex);
                }
            }
            
            visibleChunkList.Sort((s1, s2) => Vector2Int.Distance(focusPoint, s1).CompareTo(Vector2Int.Distance(focusPoint, s2)));
        }

        private void UpdateLoadChunkQueue()
        {
            foreach (Vector2Int chunkIndex in visibleChunkList)
            {
                if (!map.ContainsKey(chunkIndex) && !loadChunkQueue.Contains(chunkIndex))
                    loadChunkQueue.Enqueue(chunkIndex);
            }
        }
        
        private IEnumerator LoadChunksCoroutine()
        {
            loadChunkCoroutineRunning = true;

            int loadedChunkCount = 0;

            while (world.Config.AsyncUpdate)
            {
                if (loadChunkQueue.Count > 0)
                {
                    Vector2Int chunkIndexToLoad = loadChunkQueue.Dequeue();
                    LoadChunk(chunkIndexToLoad);
                    instantiateChunkQueue.Enqueue(chunkIndexToLoad);
                    loadedChunkCount++;
                }
                else
                {
                    yield return new WaitForSeconds(world.Config.LoadChunkInterval);
                }

                if (loadedChunkCount >= world.Config.LoadChunkBatchSize)
                {
                    loadedChunkCount = 0;
                    yield return new WaitForSeconds(world.Config.LoadChunkInterval);
                }
            }

            loadChunkCoroutineRunning = false;
        }

        private IEnumerator InstantiateChunksCoroutine()
        {
            instantiateChunkCoroutineRunning = true;

            int instantiatedChunkCount = 0;

            while (world.Config.AsyncUpdate)
            {
                if (instantiateChunkQueue.Count > 0)
                {
                    Vector2Int chunkIndexToInstantiate = instantiateChunkQueue.Dequeue();
                    InstantiateChunk(chunkIndexToInstantiate);
                    instantiatedChunkCount++;
                }
                else
                {
                    yield return new WaitForSeconds(world.Config.InstantiateChunkInterval);
                }

                if (instantiatedChunkCount >= world.Config.InstantiateChunkBatchSize)
                {
                    instantiatedChunkCount = 0;
                    yield return new WaitForSeconds(world.Config.InstantiateChunkInterval);
                }
            }

            instantiateChunkCoroutineRunning = false;
        }

        private IEnumerator UpdateChunksCoroutine()
        {
            updateChunkCoroutineRunning = true;

            int updatedChunkCount = 0;

            while (world.Config.AsyncUpdate)
            {
                if (updateChunkQueue.Count > 0)
                {
                    Vector2Int chunkIndexToUpdate = updateChunkQueue.Dequeue();
                    UpdateChunk(chunkIndexToUpdate);
                    updatedChunkCount++;
                }
                else
                {
                    yield return new WaitForSeconds(world.Config.UpdateChunkInterval);
                }

                if (updatedChunkCount >= world.Config.UpdateChunkBatchSize)
                {
                    updatedChunkCount = 0;
                    yield return new WaitForSeconds(world.Config.UpdateChunkInterval);
                }
            }

            updateChunkCoroutineRunning = false;
        }

        private void LoadChunk(Vector2Int chunkIndex)
        {
            Chunk chunk = map.CreateChunkAt(chunkIndex);
            worldGenerator.GenerateBaseTerrain(chunk);
        }
        
        private void InstantiateChunk(Vector2Int chunkIndex)
        {
            Chunk chunk = map[chunkIndex];

            GameObject chunkObject = new GameObject(world.transform.childCount + " " + chunk.Index, typeof(MeshFilter), typeof(MeshRenderer), typeof(MeshCollider));
            chunkObject.GetComponent<MeshRenderer>().material = (Material)Resources.Load("Grass", typeof(Material));
            chunkObject.transform.localPosition = new Vector3(chunkIndex.x * chunk.Size, -chunk.Height / 2, chunkIndex.y * chunk.Size);

            ChunkMesh chunkMesh = new ChunkMesh(map, chunk);
            chunkObject.GetComponent<MeshFilter>().mesh = chunkMesh.Mesh;
            chunkObject.GetComponent<MeshCollider>().sharedMesh = chunkMesh.Mesh;
            chunkObject.transform.SetParent(world.transform, true);

            chunkObjectMap.Add(chunkIndex, chunkObject);
        }

        private void UpdateChunk(Vector2Int chunkIndex)
        {
            Chunk chunk = map[chunkIndex];
            ChunkMesh chunkMesh = new ChunkMesh(map, chunk);

            GameObject chunkObject = chunkObjectMap[chunkIndex];

            chunkObject.GetComponent<MeshFilter>().mesh = chunkMesh.Mesh;
            chunkObject.GetComponent<MeshCollider>().sharedMesh = chunkMesh.Mesh;
        }
    }
}
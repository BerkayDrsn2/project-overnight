﻿using UnityEngine;

namespace ProjectOvernight.WorldEngine
{
    public class Chunk
    {
        #region STATICS
        private static byte SIZE = 32;
        private static byte HEIGHT = 32;
        #endregion

        #region COMPONENTS
        #endregion

        #region GAMEOBJECTS
        #endregion

        #region PROPERTIES
        public Vector2Int Index
        {
            get { return index; }
        }

        public byte Size
        {
            get { return SIZE; }
        }

        public byte Height
        {
            get { return HEIGHT; }
        }
        #endregion

        #region VARIABLES
        private readonly Vector2Int index;
        private Block[,,] blockMatrix;
        #endregion

        #region INDEXERS
        public Block this[Vector3Int blockIndex]
        {
            get { return blockMatrix[blockIndex.x, blockIndex.y, blockIndex.z]; }
            set { blockMatrix[blockIndex.x, blockIndex.y, blockIndex.z] = value; }
        }

        public Block this[int x, int y, int z]
        {
            get { return blockMatrix[x, y, z]; }
            set { blockMatrix[x, y, z] = value; }
        }
        #endregion

        public Chunk(Vector2Int index)
        {
            this.index = index;
            Initialize();
        }

        private void Initialize()
        {
            blockMatrix = new Block[Size, Height, Size];

            for (byte y = 0; y < Height; y++)
            {
                for (byte z = 0; z < Size; z++)
                {
                    for (byte x = 0; x < Size; x++)
                    {
                        blockMatrix[x, y, z] = new Block(1);
                    }
                }
            }
        }
    }
}

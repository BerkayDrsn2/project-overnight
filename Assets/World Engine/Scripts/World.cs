﻿using UnityEngine;

namespace ProjectOvernight.WorldEngine
{
    public class World : MonoBehaviour
    {
        #region STATICS
        #endregion

        #region COMPONENTS
        [SerializeField] public WorldEngineConfig Config;

        private Map map;
        private ChunkManager chunkManager;
        private WorldGenerator worldGenderator;
        #endregion

        #region GAMEOBJECTS
        #endregion

        #region PROPERTIES
        public int SurfaceLevel
        {
            get { return surfaceLevel; }
        }
        #endregion

        #region VARIABLES
        [SerializeField] private int surfaceLevel;
        #endregion

        void Awake()
        {

        }

        void Start()
        {
            map = new Map();
            worldGenderator = new WorldGenerator(this);
            chunkManager = new ChunkManager(this, worldGenderator);
        }

        void Update()
        {
            chunkManager.Update();
        }

        public Map GetMap()
        {
            return map;
        }
    }
}
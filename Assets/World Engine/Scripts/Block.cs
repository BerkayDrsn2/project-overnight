﻿using UnityEngine;

namespace ProjectOvernight.WorldEngine
{
    public class Block
    {
        #region STATICS
        #endregion

        #region COMPONENTS
        #endregion

        #region GAMEOBJECTS
        #endregion

        #region PROPERTIES
        public byte ID
        {
            get { return id; }
            set { id = value; }
        }
        #endregion

        #region VARIABLES
        private byte id;
        #endregion

        public Block(byte id)
        {
            this.id = id;
        }
    }
}
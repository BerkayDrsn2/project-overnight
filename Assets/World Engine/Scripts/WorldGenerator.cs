﻿using UnityEngine;

namespace ProjectOvernight.WorldEngine
{
    public class WorldGenerator
    {
        #region STATICS
        #endregion

        #region COMPONENTS
        private World world;
        #endregion

        #region GAMEOBJECTS
        #endregion

        #region PROPERTIES
        #endregion

        #region VARIABLES
        public float Scale = 200;
        public int Octave = 1;
        public float Seed = 1;
        #endregion

        public WorldGenerator(World world)
        {
            this.world = world;
        }

        public void GenerateBaseTerrain(Chunk chunk)
        {
            for (int y = 0; y < chunk.Height; y++)
            {
                for (int z = 0; z < chunk.Size; z++)
                {
                    for (int x = 0; x < chunk.Size; x++)
                    {
                        if (y < world.SurfaceLevel)
                            chunk[x, y, z].ID = 1;
                        else
                            chunk[x, y, z].ID = 0;
                    }
                }
            }
        }
    }
}
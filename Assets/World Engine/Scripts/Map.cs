﻿using System.Collections.Generic;
using UnityEngine;

namespace ProjectOvernight.WorldEngine
{
    public class Map
    {
        #region STATICS
        #endregion

        #region COMPONENTS
        #endregion

        #region GAMEOBJECTS
        #endregion

        #region PROPERTIES
        #endregion

        #region VARIABLES
        private readonly Dictionary<Vector2Int, Chunk> chunkMap;
        #endregion

        #region INDEXERS
        public Chunk this[Vector2Int index]
        {
            get { return chunkMap.ContainsKey(index) ? chunkMap[index] : null; }
        }
        #endregion

        public Map()
        {
            chunkMap = new Dictionary<Vector2Int, Chunk>();
        }

        public bool ContainsKey(Vector2Int index)
        {
            return chunkMap.ContainsKey(index);
        }

        public bool ContainsChunk(Chunk chunk)
        {
            return chunkMap.ContainsValue(chunk);
        }

        public Chunk CreateChunkAt(Vector2Int index)
        {
            Chunk chunk = new Chunk(index);
            chunkMap.Add(index, chunk);
            return chunk;
        }
    }
}
